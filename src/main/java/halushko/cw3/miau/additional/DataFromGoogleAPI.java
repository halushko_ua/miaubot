package halushko.cw3.miau.additional;

import org.apache.log4j.Logger;

import java.util.*;

public final class DataFromGoogleAPI{
    private final static Logger LOG = Logger.getLogger(DataFromGoogleAPI.class);
    private final Map<TriggersRange, Map<Integer, String>> values = new HashMap<>();
    private int length = -1;

    public DataFromGoogleAPI(Collection columns) {
        if (columns != null) {
            for (Object element : columns) {
                if (element instanceof String) {
                    values.put(TriggersRange.getEnumValue((String) element), new TreeMap<>());
                } else if (element instanceof TriggersRange) {
                    values.put((TriggersRange) element, new TreeMap<>());
                }
            }
        }
    }

    public void put(String column, String value, int index) throws MiauException {
        put(TriggersRange.getEnumValue(column), value, index);
    }

    public int getLength(){
        return length;
    }

    public void put(TriggersRange column, String value, int index) throws MiauException {
        if (!values.containsKey(column)) {
            String error = "Нет такой колонки [" + column + "] в таблице";
            LOG.error(error);
            throw new MiauException(error);
        }
        if (index >= length) {
            length = index+1;
        }

        Map<Integer, String> columnInTable = values.get(column);
        if (columnInTable.containsKey(index)) {
            LOG.warn("Поле с таким индексом [" + index +
                    "] в колонке [" + column + "] уже присутствует и равно [" +
                    columnInTable.get(index) + "]. Оно заменется значением [" + value + "]");
        }
        columnInTable.put(index, value);
    }

    public String get(TriggersRange column, int rowNumber) throws MiauException {
        if (rowNumber > length - 1) {
            String error = "Row number is too long";
            LOG.error(error);
            throw new MiauException(error);
        }
        if (!values.containsKey(column)) {
            String error = "Нет такой колонки [" + column + "] в таблице";
            LOG.error(error);
            throw new MiauException(error);
        }
        return values.get(column).getOrDefault(rowNumber, "");
    }

    public Map<TriggersRange, String> get(int rowNumber) throws MiauException {
        if (rowNumber > length) {
            String error = "Row number is too long";
            LOG.error(error);
            throw new MiauException(error);
        }
        return new HashMap<TriggersRange, String>(){
            {
                for (TriggersRange a : values.keySet()) {
                    put(a, values.get(a).getOrDefault(rowNumber, ""));
                }
            }};
    }
}
