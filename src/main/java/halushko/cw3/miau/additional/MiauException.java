package halushko.cw3.miau.additional;

public class MiauException extends Exception {
    public MiauException(String s) {
        super(s);
    }
}
