package halushko.cw3.miau.additional;

import org.apache.log4j.Logger;

import java.util.HashMap;
import java.util.Map;

public enum ReplyTypes {
    NO("НЕТ"),
    YES("ДА"),
    DOUBLE("ДВОЙНАЯ");

    final String strInExcel;
    private static Map<String, ReplyTypes> entities = new HashMap<>();
    private final static Logger LOG =  Logger.getLogger(ReplyTypes.class);

    ReplyTypes(String text) {
        strInExcel = text;
    }

    @Override
    public String toString() {
        return strInExcel;
    }

    public static ReplyTypes getReplyType(String strValue){
        if(entities.isEmpty()){
            for(ReplyTypes val: values()){
                entities.put(val.toString(), val);
            }
        }
        ReplyTypes result = entities.get(strValue);
        if(result == null){
            LOG.error("Unable to find such trigger type [" + strValue + "]");
        }
        return result;
    }
}
