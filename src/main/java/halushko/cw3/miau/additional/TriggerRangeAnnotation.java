package halushko.cw3.miau.additional;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface TriggerRangeAnnotation {
    TriggersRange range();
    boolean isJSON() default false;
    String jsonKey() default "value";
    Class type() default String.class;
    Class type2() default String.class;
    String delimiter() default "\\s+";
    String defaultValue() default "";
}
