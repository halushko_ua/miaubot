package halushko.cw3.miau.additional;

import org.apache.log4j.Logger;

import java.util.HashMap;
import java.util.Map;

public enum TriggerTypes {
    TEXT("Текст"),
    GIF("GIF"),
    IMAGE("Картинка"),
    URL("URL"),
    ROYALE("Аня");

    final String strInExcel;
    private static Map<String, TriggerTypes> entities = new HashMap<>();
    private final static Logger LOG =  Logger.getLogger(TriggerTypes.class);

    TriggerTypes(String text) {
        strInExcel = text;
    }

    @Override
    public String toString() {
        return strInExcel;
    }

    public static TriggerTypes getTriggerType(String strValue){
        if(entities.isEmpty()){
            for(TriggerTypes val: values()){
                entities.put(val.toString(), val);
            }
        }
        TriggerTypes result = entities.get(strValue);
        if(result == null){
            LOG.error("Unable to find such trigger type [" + strValue + "]");
        }
        return result;
    }
}
