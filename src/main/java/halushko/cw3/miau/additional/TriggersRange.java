package halushko.cw3.miau.additional;

import org.apache.log4j.Logger;

import java.util.Map;
import java.util.TreeMap;

public enum TriggersRange {
    TYPE("A"),
    TRIGGER("B"),
    REGEXP("C"),
    ANSWER("D"),
    COMMENT("E"),
    USER_ID("F"),
    CHAT_ID("G"),
    FROM_TIME("H"),
    TO_TIME("I"),
    ALLOWED_FREQUENCY("J"),
    NEED_ANSWER("K");

    String columnID;
    private static final Map<String, TriggersRange> entities = new TreeMap<>();
    private final static Logger LOG =  Logger.getLogger(TriggersRange.class);

    TriggersRange(String columnID){
        this.columnID = columnID;
    }

    public static TriggersRange getEnumValue(String columnID){
        if(!entities.containsKey(columnID)){
            entities.clear();
            for(TriggersRange i: values()){
                entities.put(i.columnID, i);
            }
        }
        TriggersRange result = entities.get(columnID);
        if(result == null){
            LOG.error("Unable to find such trigger type [" + columnID + "]");
        }
        return result;
    }
    public static String getStringValue(TriggersRange enumValue){
        return enumValue.toString();
    }

    @Override
    public String toString(){
        return columnID;
    }
}