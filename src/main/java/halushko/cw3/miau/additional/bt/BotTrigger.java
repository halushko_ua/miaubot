package halushko.cw3.miau.additional.bt;

import halushko.cw3.miau.additional.ReplyTypes;
import halushko.cw3.miau.additional.TriggerRangeAnnotation;
import halushko.cw3.miau.additional.TriggerTypes;
import halushko.cw3.miau.additional.TriggersRange;
import org.apache.log4j.Logger;

import java.lang.reflect.Field;
import java.math.BigInteger;
import java.util.*;

import static halushko.cw3.miau.additional.TriggersRange.*;

import org.json.*;

public abstract class BotTrigger {
    protected final static Logger LOG = Logger.getLogger(BotTrigger.class);

    @TriggerRangeAnnotation(range = TYPE, type = TriggerTypes.class)
    public TriggerTypes type;

    @TriggerRangeAnnotation(range = TRIGGER)
    public String trigger;

    @TriggerRangeAnnotation(range = REGEXP)
    public String regexp;

    @TriggerRangeAnnotation(range = USER_ID, type = Set.class, delimiter = "\\s*,+\\s*")
    public Set<String> userId;

    @TriggerRangeAnnotation(range = CHAT_ID, type = Set.class, type2 = Long.class, delimiter = "\\s*,+\\s*")
    public Set<Long> chatId;

    @TriggerRangeAnnotation(range = FROM_TIME, type = Integer.class, defaultValue = "0")
    public Integer fromTime;

    @TriggerRangeAnnotation(range = TO_TIME, type = Integer.class, defaultValue = "60")
    public Integer toTime;

    @TriggerRangeAnnotation(range = ALLOWED_FREQUENCY, type = Integer.class, defaultValue = "0")
    public Integer allowedFrequency;

    @TriggerRangeAnnotation(range = NEED_ANSWER, type = ReplyTypes.class, defaultValue = "НЕТ")
    public ReplyTypes needReply;
    public Set<Integer> replyOnlyIds = new HashSet<>();

    public Date lastRunWhen;

    public String additionalComment = "";

    public BotTrigger(Map<TriggersRange, String> values) {
        for (Field field : getAllFields()) {
            TriggerRangeAnnotation annotation = field.getDeclaredAnnotation(TriggerRangeAnnotation.class);
            if (annotation != null && values.containsKey(annotation.range())) {
                setFieldValue(annotation, field, values.get(annotation.range()));
            }
        }
    }

    protected abstract Field[] getAllFields();

    private void setFieldValue(TriggerRangeAnnotation annotation, Field field, String value) {
        if(value == null || value.equals("")){
            if(!"".equals(annotation.defaultValue())){
                setFieldValue(annotation, field, annotation.defaultValue());
                return;
            }
        }
//        LOG.debug("Try to set Field [" + field + "] value [" + value +"] annotation [" + annotation + "]");
        try {
            if (!annotation.isJSON()) {
                setNotJSON(annotation, field, value);
            } else {
                setJSON(annotation, field, value);
            }
        } catch (Exception e) {
            LOG.error("Can't set value [" + value + "] to field [" + field.getName() + "]", e);
        }
    }

    private void setJSON(TriggerRangeAnnotation annotation, Field field, String value) throws IllegalAccessException {
        JSONObject jsonObject = new JSONObject(value);
        if(!jsonObject.keySet().contains(annotation.jsonKey())){
            LOG.error("JSON [" + value + "] doesn't contains key [" + annotation.jsonKey() + "] in Column [" + annotation.range() +"]");
        } else {
            setNotJSON(annotation, field, String.valueOf(jsonObject.get(annotation.jsonKey())));
        }

    }

    private void setNotJSON(TriggerRangeAnnotation annotation, Field field, String value) throws IllegalAccessException {
        if (String.class.equals(annotation.type())) {
            setString(field, value);
        } else if (Long.class.equals(annotation.type())) {
            setLong(field, value);
        } else if (Integer.class.equals(annotation.type())) {
            setInteger(field, value);
        } else if (TriggerTypes.class.equals(annotation.type())) {
            setTriggerType(field, value);
        } else if (Set.class.equals(annotation.type())) {
            setSet(field, value, annotation);
        } else if (ReplyTypes.class.equals(annotation.type())) {
            setBool(field, value);
        }
        else {
            LOG.error("Такой тип не поддерживается как параметр: [" + annotation.type() + "]");
            field.set(this, "");
        }
    }

    private void setString(Field field, String value) throws IllegalAccessException {
        field.set(this, value);
    }
    private void setLong(Field field, String value) throws IllegalAccessException {
        field.set(this, value == null || "".equals(value) ? 0 : new Long(value));
    }
    private void setInteger(Field field, String value) throws IllegalAccessException {
        field.set(this, value == null || "".equals(value) ? 0 : new Integer(value));
    }
    private void setTriggerType(Field field, String value) throws IllegalAccessException {
        field.set(this, value == null || "".equals(value) ? TriggerTypes.TEXT : TriggerTypes.getTriggerType(value));
    }
    private void setSet(Field field, String value, TriggerRangeAnnotation annotation) throws IllegalAccessException {
        if (value == null || "".equals(value)) {
            field.set(this, Collections.EMPTY_SET);
        } else if (Long.class.equals(annotation.type2())) {
            field.set(this, new HashSet<Long>() {{
                for (String str : value.split(annotation.delimiter())) {
                    this.add(new Long(str));
                }
            }});
        } else {
            field.set(this, new HashSet<String>() {{
                Collections.addAll(this, value.split(annotation.delimiter()));
            }});
        }
    }

    private void setBool(Field field, String value) throws IllegalAccessException {
        field.set(this, ReplyTypes.getReplyType(value));
    }
}
