package halushko.cw3.miau.additional.bt;

import halushko.cw3.miau.additional.TriggerRangeAnnotation;
import halushko.cw3.miau.additional.TriggersRange;

import java.lang.reflect.Field;
import java.math.BigInteger;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static halushko.cw3.miau.additional.TriggersRange.ANSWER;

public class BotTriggerPics extends BotTriggerText {
    @TriggerRangeAnnotation(range = ANSWER, type = Set.class, delimiter = "\\s*,+\\s*")
    public Set<String> pics;

    @TriggerRangeAnnotation(range = ANSWER, isJSON = true, jsonKey = "pic")
    public String jsonPic;

    @TriggerRangeAnnotation(range = ANSWER, isJSON = true, jsonKey = "text")
    public String picComment;

    @TriggerRangeAnnotation(range = ANSWER, isJSON = true, jsonKey = "replyOnly")
    public String isReply;

    public BotTriggerPics(Map<TriggersRange, String> values) {
        super(values);
        answer = "";
        if(jsonPic != null) {
            if(pics == null) pics = new HashSet<>();
            pics.clear();
            pics.add(jsonPic);
            additionalComment = picComment;
            if(isReply != null){
                for(String id: isReply.split("\\s*,+\\s*")){
                    Integer newId = 0;
                    try{
                        newId = new Integer(id);
                    } catch (Exception e){
                        LOG.error("Exception ID of user is incorrect");
                    }
                    replyOnlyIds.add(newId);
                }
            }
        }

    }

    @Override
    protected Field[] getAllFields() {
        return BotTriggerPics.class.getFields();
    }
}
