package halushko.cw3.miau.additional.bt;

import halushko.cw3.miau.additional.TriggerRangeAnnotation;
import halushko.cw3.miau.additional.TriggersRange;

import java.lang.reflect.Field;
import java.util.Map;

import static halushko.cw3.miau.additional.TriggersRange.ANSWER;

public class BotTriggerText extends BotTrigger {
    @TriggerRangeAnnotation(range = ANSWER)
    public String answer;

    public BotTriggerText(Map<TriggersRange, String> values) {
        super(values);
    }

    @Override
    protected Field[] getAllFields() {
        return BotTriggerText.class.getFields();
    }
}
