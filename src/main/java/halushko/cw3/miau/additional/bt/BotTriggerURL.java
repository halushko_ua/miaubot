package halushko.cw3.miau.additional.bt;

import halushko.cw3.miau.additional.MiauException;
import halushko.cw3.miau.additional.TriggerRangeAnnotation;
import halushko.cw3.miau.additional.TriggersRange;

import java.lang.reflect.Field;
import java.util.Map;

import static halushko.cw3.miau.additional.TriggersRange.ANSWER;

public class BotTriggerURL extends BotTriggerText {
    @TriggerRangeAnnotation(range = ANSWER, isJSON = true, jsonKey = "URL")
    public String url;

    @TriggerRangeAnnotation(range = ANSWER, isJSON = true, jsonKey = "from")
    public String fromText;

    @TriggerRangeAnnotation(range = ANSWER, isJSON = true, jsonKey = "to")
    public String toText;

    public BotTriggerURL(Map<TriggersRange, String> values) {
        super(values);
        answer = "";
    }

    @Override
    protected Field[] getAllFields() {
        return BotTriggerURL.class.getFields();
    }
}
