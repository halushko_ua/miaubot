package halushko.cw3.miau.bot;

import halushko.cw3.miau.bot.skills.implementation.*;
import org.apache.log4j.Logger;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;

public class Controller {
    private final static Logger LOG = Logger.getLogger(Controller.class);

    public final static GalunCW3BotMiauBot BOT = new GalunCW3BotMiauBot();

    public static boolean parseMessage(Update update) {
        if (update == null) return false;
        Message msg = update.getMessage();
        if (msg != null) {
//            LOG.debug("Start parse message [ " + msg.getChatId() + "::" + msg.getChat().getTitle() + "].[" + msg.getFrom().getId() + "::" + msg.getFrom().getUserName() + "].[" + getMessage(msg) + "]");
            return parseCommand(msg) || parseTextMessage(msg);
        } else {
            LOG.debug("Message [" + update.toString() + "] is null");
            return false;
        }
    }

    private static String getMessage(Message msg) {
        StringBuilder sb = new StringBuilder();
        if (msg.getText() != null) {
            sb.append("Text=[").append(msg.getText()).append("] ");
        }
        if (msg.getDocument() != null) {
            sb.append("Gif[").append(msg.getDocument().getFileId()).append("] ");
        }
        if (msg.getPhoto() != null && msg.getPhoto().size() > 0) {
            sb.append("Photo[").append(msg.getPhoto().get(msg.getPhoto().size() - 1).getFileId()).append("] ");
        }
        return sb.toString();
    }

    private static boolean parseCommand(Message msg) {
        if (msg == null) return false;
        String text = msg.getText() == null ? "" : msg.getText();

        if (text.startsWith("/start@CWMiauBot")) {
            BOT.sendTextMsg(msg, "Мяу, мяу! Жрать дай!!!");
        }
        if (text.equals("/reload@CWMiauBot")) {
            new ReloadTriggersSkill().execute(msg);
        }
//        if (text.equals("/ave_siski")) {
//            new SendSiskiSkill().execute(msg);
//        }
        if (text.equals("/show_miau@CWMiauBot")) {
            new ShowTriggersListSkill().execute(msg);
        } else {
            return false;
        }
        return true;
    }

    private static boolean parseTextMessage(Message msg) {
        return msg != null && (
                new ShowMediaIDSkill().execute(msg)
                        || new TriggerSkill().execute(msg)
        );
    }
}
