package halushko.cw3.miau.bot;


import halushko.cw3.miau.additional.ReplyTypes;
import halushko.cw3.miau.bot.recipe.Actions;
import org.apache.log4j.Logger;
import org.telegram.telegrambots.ApiContextInitializer;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.api.methods.send.SendDocument;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.send.SendPhoto;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.DeleteMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

public class GalunCW3BotMiauBot extends TelegramLongPollingBot {
    private final static Logger LOG = Logger.getLogger(GalunCW3BotMiauBot.class);
    private final static int WAIT_TIME_SEC = 5;

    public static void main(String[] args) {
        Actions.generateRecipes();
        ApiContextInitializer.init();
        TelegramBotsApi botapi = new TelegramBotsApi();

        try {
            botapi.registerBot(Controller.BOT);
            LOG.debug("Bot started");
        } catch (TelegramApiException e) {
            LOG.error("Bot start has been fail", e);
        }
    }

    public Message sendTextMsg(Message msg, String text) {
        return sendTextMsg(msg, text, ReplyTypes.NO);
    }
    public Message sendTextMsg(Message msg, String text, ReplyTypes needReply) {
        try {
            SendMessage send = new SendMessage().setChatId(msg.getChatId());
            switch (needReply){
                case YES: send.setReplyToMessageId(msg.getMessageId()); break;
                case DOUBLE:
                    if(msg.getReplyToMessage() != null) {
                        send.setReplyToMessageId(msg.getReplyToMessage().getMessageId()); break;
                    } else {
                        return null;
                    }
                default: break;
            }
            send.setText(text.trim());
            return execute(send);
        } catch (Exception e) {
            LOG.error("Can't send Text message: ", e);
            return null;
        }
    }

    public Message sendGIFMsg(Message msg, String gif, String comment, ReplyTypes needReply) {
        try {
            SendDocument send = new SendDocument();
            send.setChatId(msg.getChatId());
            send.setDocument(gif);
            send.setCaption(comment.trim());
            switch (needReply){
                case YES: send.setReplyToMessageId(msg.getMessageId()); break;
                case DOUBLE:
                    if(msg.getReplyToMessage() != null) {
                        send.setReplyToMessageId(msg.getReplyToMessage().getMessageId());
                    } else {
                        return null;
                    }
                    break;
                default: break;
            }
            return execute(send);
        } catch (Exception e) {
            LOG.error("Can't send GIF message: ", e);
            return null;
        }
    }

    public Message sendImageMsg(Message msg, String gif, String comment) {
        try {
            return execute(new SendPhoto().setChatId(msg.getChatId()).setPhoto(gif).setCaption(comment));
        } catch (Exception e) {
            LOG.error("Can't send Photo message: ", e);
            return null;
        }
    }


    public void removeMessage(Message msg) {
        try {
            execute(new DeleteMessage(msg.getChatId(), msg.getMessageId()));
        } catch (TelegramApiException e) {

        }
    }

    @Override
    public void onUpdateReceived(Update update) {
//        sendTextMsg(update.getMessage(), "Hello!");
        Richik.parse(update);
    }

    @Override
    public String getBotUsername() {
        return "HalushkoRichikBot";
    }

    @Override
    public String getBotToken() {
        return "551534629:AAH3uMvD-K1yQM7mydl3TuedzVCOnLO9wAM";
    }
}