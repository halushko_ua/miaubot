package halushko.cw3.miau.bot;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import halushko.cw3.miau.bot.recipe.Actions;
import halushko.cw3.miau.bot.recipe.Ingredient;
import halushko.cw3.miau.bot.recipe.Product;
import org.bson.Document;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;

import java.util.HashSet;
import java.util.Set;

public final class Richik {
    public static void parse(Update update) {
        logMessageToDB(update.getMessage());
        Actions.findRecipe(update);
    }

    private static void logMessageToDB(Message message) {
        MongoClient mongoClient = new MongoClient("localhost", 27017);
        MongoDatabase database = mongoClient.getDatabase("RichikDB");
        MongoCollection<Document> a = database.getCollection("RichikLog");

        Document doc = new Document();

        doc.append("messageText", message.getText());
        doc.append("messageId", message.getMessageId());
        doc.append("chatId", message.getChatId());
        doc.append("time", message.getDate());
        doc.append("userId", message.getFrom().getId());
        doc.append("picId", message.getPhoto());
        doc.append("sticker", message.getSticker() != null ? message.getSticker().getFileId() : null);
        doc.append("document", message.getDocument() != null ? message.getDocument().getFileId() : null);
        doc.append("caption", message.getCaption());

        a.insertOne(doc);
        mongoClient.close();
    }
}
