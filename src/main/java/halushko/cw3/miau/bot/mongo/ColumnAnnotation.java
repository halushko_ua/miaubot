package halushko.cw3.miau.bot.mongo;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface ColumnAnnotation {
   String dbColumn();
   boolean isIndex() default false;
   boolean isUnique() default false;
}
