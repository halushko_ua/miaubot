package halushko.cw3.miau.bot.mongo;


import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

import javax.validation.constraints.NotNull;
import java.util.HashMap;
import java.util.Map;

public abstract class DBClass {
    private static final Map<String, MongoClient> clients = new HashMap<>();
    private static final Map<String, MongoDatabase> databases = new HashMap<>();


    protected final MongoClient mongoClient = new MongoClient("localhost", 27017);
    protected final MongoDatabase database = mongoClient.getDatabase("VareshkaQueue");
    protected final MongoCollection<Document> a =  database.getCollection("Test1");

    protected DBClass(){

    }

    private MongoClient getMongoClient(){
        String host = getMongoClientHost();
        int port = getMongoClientPort();
        String key = host + ":" + port;

        if(clients.containsKey(key)){
            return clients.get(key);
        }

        MongoClient mongoClient = new MongoClient(host, port);
        clients.put(key, mongoClient);
        return mongoClient;
    }

//    private MongoDatabase getMongoDatabase(){
//
//        MongoDatabase database = mongoClient.getDatabase("VareshkaQueue");
//    }

    protected abstract @NotNull String getMongoClientHost();
    protected abstract int getMongoClientPort();
    protected abstract String getMongoDatabaseName();
    protected abstract String getMongoMongoCollection();
}
