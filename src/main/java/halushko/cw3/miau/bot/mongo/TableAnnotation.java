package halushko.cw3.miau.bot.mongo;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface TableAnnotation {
    String host() default "localhost";
    int port() default 27017;
    String databaseName();
    String collection();
}
