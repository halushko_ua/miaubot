package halushko.cw3.miau.bot.recipe;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import halushko.cw3.miau.additional.ReplyTypes;
import org.bson.Document;
import org.telegram.telegrambots.meta.api.objects.Update;

import javax.validation.constraints.NotNull;
import java.util.*;

import static halushko.cw3.miau.bot.Controller.BOT;

public final class Actions {


    public static void findRecipe(Update update) {
        String text = update.getMessage().getText();

        MongoClient mongoClient = new MongoClient("localhost", 27017);
        MongoDatabase database = mongoClient.getDatabase("RichikDB");
        MongoCollection<Document> productsCollection = database.getCollection("Products");
        MongoCollection<Document> ingredientsCollection = database.getCollection("Ingredients");
        MongoCollection<Document> recipesCollection = database.getCollection("Recipe");

        HashMap<String, Product> products = new HashMap<>();
        HashMap<String, Ingredient> ingredients = new HashMap<>();
        HashMap<String, Map<String, Recipe>> recipes = new HashMap<>();

        for (Document doc : productsCollection.find(new Document())) {
            Product a = new Product(doc);
            products.put(a.code, a);
        }
        for (Document doc : ingredientsCollection.find(new Document())) {
            Ingredient a = new Ingredient(doc);
            ingredients.put(a.code, a);
        }
        for (Document doc : recipesCollection.find(new Document())) {
            Recipe a = new Recipe(doc);
            if (!recipes.containsKey(a.productCode)) {
                recipes.put(a.productCode, new TreeMap<>());
            }
            recipes.get(a.productCode).put(a.ingredientCode, a);
        }

        mongoClient.close();

        BOT.sendTextMsg(
                update.getMessage(),
                generateAnswer(products, ingredients, recipes, text),
                ReplyTypes.YES
        );
    }

    private static String generateAnswer(HashMap<String, Product> products, HashMap<String, Ingredient> ingredients, HashMap<String, Map<String, Recipe>> recipes, @NotNull String text) {
        String input = text.toLowerCase().replace("/", "");
        StringBuilder sb = new StringBuilder();

        if (products.containsKey(input)) {
            sb.append(printProduct(products.get(input), products, ingredients, recipes));
        } else if (ingredients.containsKey(input)) {
            sb.append(printIngredient(ingredients.get(input), products, recipes));
        } else {
            sb.append(ifNotCode(products, ingredients, input));
        }
        return sb.toString().trim();

    }

    private static StringBuilder ifNotCode(HashMap<String, Product> products, HashMap<String, Ingredient> ingredients, String input) {
        StringBuilder sb = new StringBuilder("Возможно Вы имели ввиду:\n");
        for (Map.Entry<String, Product> pp : products.entrySet()) {
            Set<String> keys = new HashSet<>();
            Product product = pp.getValue();
            keys.add(pp.getKey().toLowerCase());
            keys.addAll(product.synonyms);
            keys.add(product.groupName.toLowerCase());
            keys.remove("");

            outerloop:
            for (String key : keys) {
                for (int i = 0; i < input.length(); i++) {
                    char previous = ' ';
                    boolean flag = true;
                    StringBuilder reg = new StringBuilder();
                    for (int j = 0; j < input.length(); j++) {
                        char ch = input.charAt(j);
                        if (flag || previous != ch) {
                            flag = false;
                            reg.append(i == j ? '.' : ch);
                            previous = ch;
                            reg.append(i == j ? "*" : "+");
                        }
                    }
                    if (key.matches(reg.toString())) {
                        sb.append(product.name).append("  /").append(product.code).append("\n");
                        break outerloop;
                    }
                }
            }
        }


        for (Map.Entry<String, Ingredient> ii : ingredients.entrySet()) {
            Set<String> keys = new HashSet<>();
            Ingredient ingredient = ii.getValue();
            keys.add(ii.getKey().toLowerCase());
            keys.addAll(ingredient.synonyms);
            keys.remove("");

            outerloop:
            for (String key : keys) {


                for (int i = 0; i < input.length(); i++) {
                    char previous = ' ';
                    boolean flag = true;
                    StringBuilder reg = new StringBuilder();
                    for (int j = 0; j < input.length(); j++) {
                        char ch = input.charAt(j);
                        if (flag || previous != ch) {
                            flag = false;
                            reg.append(i == j ? '.' : ch);
                            previous = ch;
                            reg.append(i == j ? "*" : "+");
                        }
                    }
                    if (key.matches(reg.toString())) {
                        sb.append(ingredient.name).append("  /").append(ingredient.code).append("\n");
                        break outerloop;
                    }
                }
            }        }

        return sb;
    }

    private static StringBuilder printProduct(Product product, HashMap<String, Product> products, HashMap<String, Ingredient> ingredients, HashMap<String, Map<String, Recipe>> recipes) {
        StringBuilder sb = new StringBuilder();
        sb.append(product.name).append(" \uD83D\uDC8E ").append(product.mana).append("\n");

        for (String ingredientCode : recipes.get(product.code).keySet()) {
            Recipe recipe = recipes.get(product.code).get(ingredientCode);
            if (products.containsKey(ingredientCode)) {
                Product ingredient = products.get(ingredientCode);
                sb.append(" ").append(recipe.count).append(" x ");
                sb.append(ingredient.name).append(" /").append(ingredient.code).append("\n");
            }
            if (ingredients.containsKey(ingredientCode)) {
                Ingredient ingredient = ingredients.get(ingredientCode);
                sb.append(" ").append(recipe.count).append(" x ");
                sb.append(ingredient.name).append(" /").append(ingredient.code).append("\n");
            }
        }
        return sb.append(printParentProducts(product.code, products, recipes));
    }

    private static StringBuilder printIngredient(Ingredient ingredient, Map<String, Product> products, HashMap<String, Map<String, Recipe>> recipes) {
        StringBuilder sb = new StringBuilder();
        sb.append(ingredient.name).append("\n");
        sb.append("harvest lvl: ").append(ingredient.harvest).append(", time: ").append(ingredient.time).append("\n");

        return sb.append(printParentProducts(ingredient.code, products, recipes));
    }

    private static StringBuilder printParentProducts(String ingredientCode, Map<String, Product> products, HashMap<String, Map<String, Recipe>> recipes){
        StringBuilder sb = new StringBuilder("\nВходит в:\n");

        for(Map.Entry<String, Map<String, Recipe>> recipe: recipes.entrySet()){
            if(recipe.getValue().containsKey(ingredientCode)){
                Product product = products.get(recipe.getKey());
                sb.append(" ").append(product.name).append(" /").append(product.code).append("\n");
            }
        }
        return sb;
    }

    public static void generateRecipes() {
        MongoClient mongoClient = new MongoClient("localhost", 27017);
        MongoDatabase database = mongoClient.getDatabase("RichikDB");

        MongoCollection<Document> products = database.getCollection("Products");
        products.deleteMany(new Document());
        Product.insert("509", "No Group", "Poison Pack", 10, 1, products, "поизон", "яд");
        Product.insert("507", "No Group", "Remedy Pack", 10, 1, products, "ремеди");
        Product.insert("63", "No Group", "Assassin Vine", 15, 2, products, "асасин", "ассасин", "вино");
        Product.insert("61", "No Group", "Ethereal Bone", 10, 1, products, "бон", "кость");
        Product.insert("62", "No Group", "Itacory", 15, 2, products, "итакори");
        Product.insert("64", "No Group", "Kloliarway", 15, 2, products, "клори");
        Product.insert("59", "No Group", "Plasma of Abyss", 10, 1, products, "плазма");
        Product.insert("69", "No Group", "Silver Dust", 30, 3, products, "сильвер", "даст");
        Product.insert("60", "No Group", "Ultramarine Dust", 10, 1, products, "ультрамарин", "даст");
        Product.insert("p01", "Rage", "Vial of Rage", 10, 1, products, "виаль", "рейдж", "ярость");
        Product.insert("p02", "Rage", "Potion of Rage", 10, 2, products, "потион", "рейдж", "ярость");
        Product.insert("p03", "Rage", "Bottle of Rage", 30, 3, products, "ботл", "бутылка", "рейдж", "ярость");
        Product.insert("p04", "Peace", "Vial of Peace", 10, 1, products, "виаль", "пис");
        Product.insert("p05", "Peace", "Potion of Peace", 15, 2, products, "потион", "пис");
        Product.insert("p06", "Peace", "Bottle of Peace", 30, 3, products, "ботл", "бутылка", "пис");
        Product.insert("p07", "Greed", "Vial of Greed", 10, 1, products, "виаль", "грид");
        Product.insert("p08", "Greed", "Potion of Greed", 15, 2, products, "потион", "грид");
        Product.insert("p09", "Greed", "Bottle of Greed", 30, 3, products, "ботл", "бутылка", "грид");
        Product.insert("p10", "Nature", "Vial of Nature", 10, 1, products, "виаль", "натур", "природ");
        Product.insert("p11", "Nature", "Potion of Nature", 15, 2, products, "потион", "натур", "природ");
        Product.insert("p12", "Nature", "Bottle of Nature", 30, 3, products, "ботл", "бутылка", "натур", "природ");
        Product.insert("p13", "Mana", "Vial of Mana", 15, 2, products, "мана", "виаль");
        Product.insert("p14", "Mana", "Potion of Mana", 30, 3, products, "мана", "потион");
        Product.insert("p15", "Mana", "Bottle of Mana", 50, 4, products, "мана", "ботл", "бутылка");
        Product.insert("p16", "Twilight", "Vial of Twilight", 15, 2, products, "твилайт", "виаль", "твайлайт");
        Product.insert("p17", "Twilight", "Potion of Twilight", 30, 3, products, "твилайт", "потион", "твайлайт");
        Product.insert("p18", "Twilight", "Bottle of Twilight", 50, 4, products, "твилайт", "ботл", "бутылка", "твайлайт");
        Product.insert("p19", "Morph", "Vial of Morph", 15, 2, products, "морф", "виаль");
        Product.insert("p20", "Morph", "Potion of Morph", 30, 3, products, "морф", "потион");
        Product.insert("p21", "Morph", "Bottle of Morph", 50, 4, products, "морф", "ботл", "бутылка");

        MongoCollection<Document> ingredients = database.getCollection("Ingredients");
        ingredients.deleteMany(new Document());
        Ingredient.insert("0", "Всегда", "Bone Powder", "21", ingredients, "повдер", "кость", "порошок");
        Ingredient.insert("0", "Всегда", "Powder", "7", ingredients, "повдер", "порошок");
        Ingredient.insert("0", "Всегда", "Purified Powder", "24", ingredients, "повдер", "пурифайд", "порошок");
        Ingredient.insert("0", "Всегда", "Silver Ore", "10", ingredients, "серебр", "оре");
        Ingredient.insert("1", "Ночью", "Cave Garlic", "49", ingredients, "кэйв", "гарлик");
        Ingredient.insert("1", "Днем", "Cliff Rue", "41", ingredients);
        Ingredient.insert("1", "Днем", "Ephijora", "47", ingredients);
        Ingredient.insert("1", "Днем", "Ilaves", "46", ingredients);
        Ingredient.insert("1", "Ночью", "Maccunut", "56", ingredients);
        Ingredient.insert("1", "Ночью", "Mercy Sassafras", "40", ingredients);
        Ingredient.insert("1", "Днем", "Stinky Sumac", "39", ingredients);
        Ingredient.insert("1", "Ночью", "Spring Bay Leaf", "52", ingredients);
        Ingredient.insert("1", "Неизвестно", "Storm Hyssop", "48", ingredients, "хисоп", "сторм");
        Ingredient.insert("1", "Днем", "Sun Tarragon", "55", ingredients);
        Ingredient.insert("1", "Ночью", "Tecceagrass", "51", ingredients);
        Ingredient.insert("1", "Неизвестно", "Wolf Root", "43", ingredients);
        Ingredient.insert("1", "Ночью", "Yellow Seed", "50", ingredients);
        Ingredient.insert("1", "Днем", "Ash Rosemary", "53", ingredients);
        Ingredient.insert("3", "Днем", "Queen's Pepper", "58", ingredients);
        Ingredient.insert("3", "Днем", "Sanguine Parsley", "54", ingredients);
        Ingredient.insert("3", "Ночью", "Swamp Lavender", "44", ingredients);
        Ingredient.insert("3", "Ночью", "White Blossom", "45", ingredients);
        Ingredient.insert("7", "Ночью", "Dragon Seed", "57", ingredients);
        Ingredient.insert("7", "Ночью", "Love Creeper", "42", ingredients);
        Ingredient.insert("12", "Ночью", "Mammoth Dill", "68", ingredients);
        Ingredient.insert("12", "Ночью", "Plexisop", "67", ingredients);
        Ingredient.insert("12", "Неизвестно", "Astrulic", "65", ingredients);
        Ingredient.insert("12", "Неизвестно", "Flammia Nut", "66", ingredients);

        MongoCollection<Document> recipes = database.getCollection("Recipe");
        recipes.deleteMany(new Document());

        Recipe.insert("509", "47", "1", recipes);
        Recipe.insert("509", "56", "1", recipes);

        Recipe.insert("507", "41", "1", recipes);
        Recipe.insert("507", "51", "1", recipes);

        Recipe.insert("63", "53", "1", recipes);
        Recipe.insert("63", "45", "1", recipes);

        Recipe.insert("61", "47", "1", recipes);
        Recipe.insert("61", "46", "1", recipes);
        Recipe.insert("61", "56", "1", recipes);
        Recipe.insert("61", "50", "1", recipes);

        Recipe.insert("62", "53", "1", recipes);
        Recipe.insert("62", "44", "1", recipes);

        Recipe.insert("64", "40", "1", recipes);
        Recipe.insert("64", "58", "1", recipes);

        Recipe.insert("59", "49", "1", recipes);
        Recipe.insert("59", "41", "1", recipes);
        Recipe.insert("59", "55", "1", recipes);

        Recipe.insert("69", "7", "2", recipes);
        Recipe.insert("69", "10", "5", recipes);
        Recipe.insert("69", "51", "1", recipes);

        Recipe.insert("60", "39", "1", recipes);
        Recipe.insert("60", "48", "1", recipes);
        Recipe.insert("60", "51", "1", recipes);

        Recipe.insert("p01", "48", "1", recipes);
        Recipe.insert("p01", "55", "1", recipes);

        Recipe.insert("p02", "48", "2", recipes);
        Recipe.insert("p02", "63", "1", recipes);
        Recipe.insert("p02", "59", "1", recipes);

        Recipe.insert("p03", "52", "1", recipes);
        Recipe.insert("p03", "43", "1", recipes);
        Recipe.insert("p03", "63", "2", recipes);
        Recipe.insert("p03", "59", "1", recipes);

        Recipe.insert("p04", "49", "1", recipes);
        Recipe.insert("p04", "39", "1", recipes);

        Recipe.insert("p05", "49", "2", recipes);
        Recipe.insert("p05", "62", "1", recipes);
        Recipe.insert("p05", "60", "1", recipes);

        Recipe.insert("p06", "52", "1", recipes);
        Recipe.insert("p06", "57", "1", recipes);
        Recipe.insert("p06", "62", "2", recipes);
        Recipe.insert("p06", "60", "1", recipes);

        Recipe.insert("p07", "46", "1", recipes);
        Recipe.insert("p07", "50", "1", recipes);

        Recipe.insert("p08", "61", "1", recipes);
        Recipe.insert("p08", "64", "1", recipes);

        Recipe.insert("p09", "52", "1", recipes);
        Recipe.insert("p09", "42", "1", recipes);
        Recipe.insert("p09", "61", "1", recipes);
        Recipe.insert("p09", "64", "2", recipes);

        Recipe.insert("p10", "7", "4", recipes);
        Recipe.insert("p10", "41", "1", recipes);
        Recipe.insert("p10", "47", "1", recipes);

        Recipe.insert("p11", "21", "3", recipes);
        Recipe.insert("p11", "7", "7", recipes);
        Recipe.insert("p11", "47", "3", recipes);
        Recipe.insert("p11", "59", "1", recipes);

        Recipe.insert("p12", "24", "1", recipes);
        Recipe.insert("p12", "64", "1", recipes);
        Recipe.insert("p12", "59", "3", recipes);

        Recipe.insert("p13", "7", "3", recipes);
        Recipe.insert("p13", "41", "1", recipes);
        Recipe.insert("p13", "51", "2", recipes);
        Recipe.insert("p13", "61", "1", recipes);

        Recipe.insert("p14", "7", "3", recipes);
        Recipe.insert("p14", "41", "3", recipes);
        Recipe.insert("p14", "56", "2", recipes);
        Recipe.insert("p14", "65", "1", recipes);
        Recipe.insert("p14", "62", "2", recipes);

        Recipe.insert("p15", "47", "2", recipes);
        Recipe.insert("p15", "51", "4", recipes);
        Recipe.insert("p15", "67", "1", recipes);
        Recipe.insert("p15", "61", "2", recipes);
        Recipe.insert("p15", "64", "1", recipes);

        Recipe.insert("p16", "10", "3", recipes);
        Recipe.insert("p16", "39", "1", recipes);
        Recipe.insert("p16", "51", "2", recipes);
        Recipe.insert("p16", "42", "1", recipes);

        Recipe.insert("p17", "46", "3", recipes);
        Recipe.insert("p17", "50", "2", recipes);
        Recipe.insert("p17", "65", "1", recipes);
        Recipe.insert("p17", "64", "1", recipes);
        Recipe.insert("p17", "69", "1", recipes);

        Recipe.insert("p18", "56", "3", recipes);
        Recipe.insert("p18", "39", "3", recipes);
        Recipe.insert("p18", "43", "1", recipes);
        Recipe.insert("p18", "68", "1", recipes);
        Recipe.insert("p18", "54", "1", recipes);
        Recipe.insert("p18", "69", "3", recipes);

        Recipe.insert("p19", "7", "5", recipes);
        Recipe.insert("p19", "48", "1", recipes);
        Recipe.insert("p19", "66", "1", recipes);
        Recipe.insert("p19", "63", "1", recipes);
        Recipe.insert("p19", "62", "1", recipes);

        Recipe.insert("p20", "56", "3", recipes);
        Recipe.insert("p20", "58", "1", recipes);
        Recipe.insert("p20", "68", "1", recipes);
        Recipe.insert("p20", "62", "1", recipes);
        Recipe.insert("p20", "59", "1", recipes);
        Recipe.insert("p20", "69", "2", recipes);

        Recipe.insert("p21", "47", "3", recipes);
        Recipe.insert("p21", "40", "2", recipes);
        Recipe.insert("p21", "67", "1", recipes);
        Recipe.insert("p21", "66", "1", recipes);
        Recipe.insert("p21", "63", "1", recipes);
        Recipe.insert("p21", "59", "1", recipes);
        Recipe.insert("p21", "69", "6", recipes);
        Recipe.insert("p21", "60", "1", recipes);

        Product.insert("ddd01", "Еда для Димы", "Рагу", 0, 0, products);
        Ingredient.insert("1", "В магазе есть", "Говядина", "ddd02", ingredients);
        Ingredient.insert("1", "В магазе есть", "Лук репчатый", "ddd03", ingredients);
        Ingredient.insert("1", "В магазе есть", "Морковь", "ddd04", ingredients);
        Ingredient.insert("1", "В магазе есть", "Зеленый горошек", "ddd05", ingredients);
        Ingredient.insert("1", "В магазе есть", "Цветная капуста", "ddd06", ingredients);
        Ingredient.insert("1", "В магазе есть", "Консервированная кукуруза", "ddd07", ingredients);
        Ingredient.insert("1", "В магазе есть", "Сметана", "ddd08", ingredients);
        Ingredient.insert("1", "В магазе есть", "Соль", "ddd09", ingredients);
        Ingredient.insert("1", "В магазе есть", "Свежий красный перец", "ddd10", ingredients);
        Ingredient.insert("1", "В магазе есть", "Растительное масло", "ddd11", ingredients);
        Ingredient.insert("1", "В магазе есть", "Сельдерей", "ddd12", ingredients);

        Recipe.insert("ddd01", "ddd02", "500 гр", recipes);
        Recipe.insert("ddd01", "ddd03", "3", recipes);
        Recipe.insert("ddd01", "ddd04", "5", recipes);
        Recipe.insert("ddd01", "ddd05", "300 гр", recipes);
        Recipe.insert("ddd01", "ddd06", "1", recipes);
        Recipe.insert("ddd01", "ddd07", "300 гр", recipes);
        Recipe.insert("ddd01", "ddd08", "По вкусу", recipes);
        Recipe.insert("ddd01", "ddd09", "1", recipes);
        Recipe.insert("ddd01", "ddd10", "По вкусу", recipes);
        Recipe.insert("ddd01", "ddd11", "1 ст. л.", recipes);
        Recipe.insert("ddd01", "ddd12", "2", recipes);

        Product.insert("ddd13", "Пацан", "Галушко", 999999, 99999, products, "дима", "димочка");
        Ingredient.insert("9999", "Всегда идеален", "Всегда идеален", "ddd14", ingredients);
        Recipe.insert("ddd13", "ddd14", "1 шт", recipes);


        mongoClient.close();
    }
}
