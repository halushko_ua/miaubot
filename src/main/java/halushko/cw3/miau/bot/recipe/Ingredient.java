package halushko.cw3.miau.bot.recipe;

import com.mongodb.client.MongoCollection;
import org.bson.Document;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class Ingredient {
    public final String code;
    public final String name;
    public final String time;
    public final String harvest;
    public final Set<String> synonyms = new HashSet<>();


    public Ingredient(Document doc){
        code = doc.getString("code");
        name = doc.getString("name");
        time = doc.getString("time");
        harvest = doc.getString("harvest");
        String str = doc.getString("synonyms");
        if(str != null) {
            synonyms.addAll(Arrays.asList(str.toLowerCase().trim().split(" ")));
            synonyms.addAll(Arrays.asList(name.toLowerCase().trim().split(" ")));
            synonyms.addAll(Arrays.asList(time.toLowerCase().trim().split(" ")));
            synonyms.add(name.toLowerCase());
            synonyms.add(time.toLowerCase());
        }

    }
    public static void insert(String harvest, String time, String name, String code, MongoCollection<Document> mongoCollection, String... synonyms) {
        Document doc = new Document();

        doc.append("code", code);
        doc.append("name", name);
        doc.append("time", time);
        doc.append("harvest", harvest);

        StringBuilder sb = new StringBuilder();
        for(String s: synonyms != null ? synonyms : new String[0]){
            sb.append(s).append(" ");
        }
        doc.append("synonyms", sb.toString());

        mongoCollection.insertOne(doc);
    }
}
