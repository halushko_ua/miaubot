package halushko.cw3.miau.bot.recipe;

import com.mongodb.client.MongoCollection;
import org.bson.Document;

import java.util.*;

public class Product {
    public final String code;
    public final String groupName;
    public final String name;
    public final String mana;
    public final String rank;
    public final Set<String> synonyms = new HashSet<>();

    public Product(Document doc){
        code = doc.getString("code");
        name = doc.getString("name");
        groupName = doc.getString("groupName");
        mana = doc.getString("mana");
        rank = doc.getString("rank");
        String str = doc.getString("synonyms");
        if(str != null) {
            synonyms.addAll(Arrays.asList(str.toLowerCase().trim().split(" ")));
            synonyms.addAll(Arrays.asList(name.toLowerCase().trim().split(" ")));
            synonyms.add(name.toLowerCase());
            synonyms.add(groupName.toLowerCase());
            synonyms.addAll(Arrays.asList(code.toLowerCase().trim().split(" ")));
            synonyms.addAll(Arrays.asList(groupName.toLowerCase().trim().split(" ")));
        }
    }

    public static void insert(String code, String groupName, String name, int mana, int rank, MongoCollection<Document> mongoCollection, String... synonyms) {
        Document doc = new Document();

        doc.append("code", code);
        doc.append("groupName", groupName);
        doc.append("name", name);
        doc.append("mana", String.valueOf(mana));
        doc.append("rank", String.valueOf(rank));

        StringBuilder sb = new StringBuilder();
        for(String s: synonyms != null ? synonyms : new String[0]){
            sb.append(s).append(" ");
        }
        doc.append("synonyms", sb.toString());

        mongoCollection.insertOne(doc);
    }
}
