package halushko.cw3.miau.bot.recipe;

import com.mongodb.client.MongoCollection;
import org.bson.Document;

public class Recipe {
    public final String productCode;
    public final String ingredientCode;
    public final String count;

    public Recipe(Document doc){
        productCode = doc.getString("productCode");
        ingredientCode = doc.getString("ingredientCode");
        count = doc.getString("count");
    }

    public static void insert(String productCode, String ingredientCode, String count, MongoCollection<Document> mongoCollection){
        Document doc = new Document();

        doc.append("productCode", productCode);
        doc.append("ingredientCode", ingredientCode);
        doc.append("count", count);

        mongoCollection.insertOne(doc);
    }
}
