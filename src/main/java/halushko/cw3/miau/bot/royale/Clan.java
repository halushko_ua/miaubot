package halushko.cw3.miau.bot.royale;


import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class Clan {
    public static final Clan EMPTY = new Clan();
    public Player[] members;

    public Player getMember(String tag){
        for(Player p: members){
            if(p != null && p.tag != null && p.tag.equals(tag)) return p;
        }
        return Player.EMPTY;
    }

    public Clan(){

    }
}
