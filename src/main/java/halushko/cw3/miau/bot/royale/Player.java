package halushko.cw3.miau.bot.royale;

public class Player {
    public final static Player EMPTY = new Player();

    @RoyaleAPITags(tag = "name")
    public String name;
    @RoyaleAPITags(tag = "tag")
    public String tag;
    @RoyaleAPITags(tag = "donations")
    public int donations;
    @RoyaleAPITags(tag = "donationsReceived")
    public int donationsReceived;

    public int battlesPlayed;

    public Player(){

    }
}
