package halushko.cw3.miau.bot.royale;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface RoyaleAPITags {
    String tag();
    Class type() default String.class;
    Class type2() default String.class;
}