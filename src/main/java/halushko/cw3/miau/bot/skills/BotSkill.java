package halushko.cw3.miau.bot.skills;

import org.telegram.telegrambots.meta.api.objects.Message;

public interface BotSkill {
    public boolean execute(Message msg, String... params);
}
