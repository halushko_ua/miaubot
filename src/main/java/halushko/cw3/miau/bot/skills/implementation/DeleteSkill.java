package halushko.cw3.miau.bot.skills.implementation;

import halushko.cw3.miau.bot.skills.BotSkill;
import org.telegram.telegrambots.meta.api.objects.Message;

import java.util.*;

import static halushko.cw3.miau.bot.Controller.BOT;

public class DeleteSkill implements BotSkill {
    private static final Map<Long, Set<Message>> tasks = new TreeMap<>();
    private static final Timer TIMER;

    static {
        TIMER = new Timer();
        TIMER.schedule(new TimerTask() {
                           @Override
                           public void run() {
                               long now = new Date().getTime();
                               Set<Long> remove = new HashSet<>();
                               for (Long a : tasks.keySet()) {
                                   if (now >= a) {
                                       for (Message msg : tasks.get(a)) {
                                           if (msg != null) BOT.removeMessage(msg);
                                           remove.add(a);
                                       }
                                   } else {
                                       break;
                                   }
                               }
                               for(Long key: remove){
                                   tasks.remove(key);
                               }
                           }
                       },
                new Date(),
                1000L
        );
    }


    @Override
    public boolean execute(final Message msg, String... params) {
        if(params == null || params.length < 1) return false;
        String time = params[0];
        if (time != null) {
            Long key = Long.valueOf(time) + new Date().getTime();
            if (!tasks.containsKey(key)) {
                tasks.put(key, new HashSet<>());
            }
            tasks.get(key).add(msg);
        }
        return true;
    }
}
