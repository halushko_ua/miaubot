package halushko.cw3.miau.bot.skills.implementation;

import halushko.cw3.miau.bot.Controller;
import org.apache.log4j.Logger;
import org.telegram.telegrambots.meta.api.objects.Message;

import static halushko.cw3.miau.bot.Controller.BOT;

public class ReloadTriggersSkill extends TriggerSkill {
    private final static Logger LOG = Logger.getLogger(ReloadTriggersSkill.class);

    @Override
    public boolean execute(Message msg, String... params) {
        if (msg == null) return false;
        try {
            new TriggerSkill().getTriggers(true);
            BOT.sendTextMsg(msg, "Команды обновлены");
            return true;
        } catch (Exception e) {
            LOG.error("Команды не обновлены");
            StringBuilder sb = new StringBuilder().append(String.valueOf(e.getMessage())).append("\n");

            for(StackTraceElement str: e.getStackTrace()){
                sb.append(str.getClassName()).append(".").append(str.getMethodName()).append(".").append(str.getLineNumber()).append("\n");
            }

            BOT.sendTextMsg(msg, "Команды не обновлены\n" + sb.toString());
            return false;
        }
    }
}
