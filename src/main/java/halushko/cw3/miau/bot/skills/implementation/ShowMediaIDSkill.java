package halushko.cw3.miau.bot.skills.implementation;

import halushko.cw3.miau.bot.Controller;
import halushko.cw3.miau.bot.skills.BotSkill;
import org.telegram.telegrambots.meta.api.objects.Message;

public class ShowMediaIDSkill implements BotSkill {

    @Override
    public boolean execute(Message msg, String... params) {
        if (!msg.isUserMessage()) return false;

        StringBuilder sb = new StringBuilder();
        if (msg.getText() != null) {
            sb.append("UserID=[").append(msg.getChatId()).append("] ");
        }
        if (msg.getDocument() != null) {
            sb.append("Gif[").append(msg.getDocument().getFileId()).append("] ");
        }
        if (msg.getPhoto() != null && msg.getPhoto().size() > 0) {
            sb.append("Photo[").append(msg.getPhoto().get(msg.getPhoto().size() - 1).getFileId()).append("] ");
        }
        Controller.BOT.sendTextMsg(msg, sb.toString());
        return true;
    }
}
