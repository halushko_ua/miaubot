package halushko.cw3.miau.bot.skills.implementation;

import halushko.cw3.miau.additional.ReplyTypes;
import halushko.cw3.miau.additional.bt.BotTrigger;
import halushko.cw3.miau.bot.Controller;
import org.apache.log4j.Logger;
import org.telegram.telegrambots.meta.api.objects.Message;

import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import static halushko.cw3.miau.bot.Controller.BOT;

public class ShowTriggersListSkill extends TriggerSkill {
    private final static Logger LOG = Logger.getLogger(ShowTriggersListSkill.class);

    @Override
    public boolean execute(Message msg, String... params) {
        Map<String, Set<BotTrigger>> triggers;
        try {
            triggers = new TriggerSkill().getTriggers(true);
        } catch (Exception e) {
            LOG.error("Команды не обновлены");
//            BOT.sendTextMsg(msg, "Что-то пошло не так");
            return false;
        }
        Set<String> res = new TreeSet<>();
        for (String key : triggers.keySet()) {
            if (key != null) {
                res.add(triggers.get(key).iterator().next().trigger);
            }
        }

        StringBuilder sb = new StringBuilder("Все мои триггеры: ");
        for (String key : res) {
            sb.append("\n").append(key);
        }
        BOT.sendTextMsg(msg, sb.toString());
        return true;
    }
}
