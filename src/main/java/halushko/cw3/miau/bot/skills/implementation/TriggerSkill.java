package halushko.cw3.miau.bot.skills.implementation;

import halushko.cw3.miau.additional.TriggerTypes;
import halushko.cw3.miau.additional.bt.BotTrigger;
import halushko.cw3.miau.additional.bt.BotTriggerText;
import halushko.cw3.miau.bot.royale.Clan;
import halushko.cw3.miau.bot.royale.Player;
import halushko.cw3.miau.bot.skills.BotSkill;
import halushko.cw3.miau.additional.MiauException;
import halushko.cw3.miau.services.ServicesPull;
import halushko.cw3.miau.services.TriggersGetter;
import halushko.cw3.miau.utils.RoyaleAPI;
import org.apache.log4j.Logger;
import org.telegram.telegrambots.meta.api.objects.Message;

import java.util.*;
import java.util.concurrent.TimeUnit;

import static halushko.cw3.miau.bot.Controller.BOT;

public class TriggerSkill implements BotSkill {
    private final static Logger LOG = Logger.getLogger(TriggerSkill.class);

    @Override
    public boolean execute(Message msg, String... params) {
        if (msg == null) return false;

        Map<String, Set<BotTrigger>> triggers;

        try {
            triggers = getTriggers(false);
            if (triggers.isEmpty()) {
                triggers = getTriggers(true);
            }
            sendRandomMessageByTrigger(triggers, msg);
        } catch (Exception e) {
            LOG.error("ERR: ", e);
//            BOT.sendTextMsg(msg, "Ошибка. Обратитесь ка разработчику, а то он шота тут нахомутал");
        }
        return false;
    }

    private boolean sendRandomMessageByTrigger(Map<String, Set<BotTrigger>> triggers, Message msg) {
        String text = msg.getText() == null ? "" : msg.getText();
        for (String pattern : triggers.keySet()) {
            if (text.toLowerCase().matches(pattern)) {
                Set<BotTrigger> messages = getAnswers(msg, triggers.get(pattern));
                if (messages.size() < 1) continue;
                int item = new Random().nextInt(messages.size());
                int i = -1;
                for (BotTrigger trigger : messages) {
                    if (++i == item) {
                        try {
                            TimeUnit.SECONDS.sleep(1);
                        } catch (InterruptedException e) {
                            BOT.sendTextMsg(msg, e.getMessage());
                        }
                        if (TriggerTypes.TEXT.equals(trigger.type)) {
                            if (((BotTriggerText) trigger).answer != null) {
                                BOT.sendTextMsg(msg, ((BotTriggerText) trigger).answer);
                            }
                        } else if (TriggerTypes.GIF.equals(trigger.type)) {
                            if (((BotTriggerText) trigger).answer != null) {
                                if(trigger.replyOnlyIds.isEmpty() || (msg.getReplyToMessage() != null && trigger.replyOnlyIds.contains(msg.getReplyToMessage().getFrom().getId()))){
                                    new DeleteSkill().execute(BOT.sendGIFMsg(msg, ((BotTriggerText) trigger).answer, ((BotTriggerText) trigger).additionalComment, ((BotTriggerText) trigger).needReply), "300000");
                                }
                            }
                        } else if (TriggerTypes.IMAGE.equals(trigger.type)) {
                            if (((BotTriggerText) trigger).answer != null) {
                                new DeleteSkill().execute(BOT.sendImageMsg(msg, ((BotTriggerText) trigger).answer, ((BotTriggerText) trigger).additionalComment), "300000");
                            }
                        } else if(TriggerTypes.ROYALE.equals(trigger.type)) {
                            Clan clan = RoyaleAPI.getClan("9PPV2PC9");
                            StringBuilder sb = new StringBuilder("Рекомендую:\n");
                            for(Player player: clan.members){
                                if(player.donationsReceived > 0 && player.donations < 41){
                                    sb.append(player.name).append("\n");
                                }
                            }
                            BOT.sendTextMsg(msg, sb.toString());
                        } else {
                            LOG.warn("Not implemented type of trigger [" + trigger.type + "]");
                            return false;
                        }
                        return true;
                    }
                }
            }
        }
        return false;
    }

    protected Set<BotTrigger> getAnswers(Message msg, Set<BotTrigger> triggers) {
        return new LinkedHashSet<BotTrigger>() {{
            for (BotTrigger answer : triggers) {
                if (answer.chatId.size() > 0 && !answer.chatId.contains(msg.getChatId())) continue;
                if (answer.userId.size() > 0 && !answer.userId.contains(String.valueOf(msg.getFrom().getId())) && !answer.userId.contains(msg.getFrom().getUserName()))
                    continue;

                Date now = new Date();

                Date lastRunWhen = answer.lastRunWhen == null ? new Date(now.getTime() - 1000*60*60*24) : answer.lastRunWhen;

                if (now.getMinutes() >= answer.fromTime && now.getMinutes() < answer.toTime
                        && now.getTime() - lastRunWhen.getTime() > answer.allowedFrequency*1000 ) {
                    answer.lastRunWhen = now;
                    add(answer);
                }
            }
        }};
    }

    protected Map<String, Set<BotTrigger>> getTriggers(boolean flag) throws Exception {
        try {
            TriggersGetter triggersGetter = (TriggersGetter) ServicesPull.getService(TriggersGetter.class);
            return triggersGetter.runAction(flag);
        } catch (MiauException e) {
            LOG.error("Can't get triggers list", e);
            return Collections.emptyMap();
        } catch (Exception e) {
            LOG.error("Can't get triggers list", e);
            throw e;
        }
    }
}
