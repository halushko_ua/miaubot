package halushko.cw3.miau.services;

import halushko.cw3.miau.additional.TriggersRange;
import halushko.cw3.miau.additional.bt.BotTrigger;
import halushko.cw3.miau.additional.bt.BotTriggerPics;
import halushko.cw3.miau.additional.bt.BotTriggerText;
import halushko.cw3.miau.bot.Controller;
import org.apache.log4j.Logger;

import java.util.*;

public class GIFGetterService extends Service<Set<BotTrigger>, Map<TriggersRange, String>> {
    private final static Logger LOG = Logger.getLogger(Controller.class);

    GIFGetterService(Object[] args) {
        super(args);
    }

    @Override
    protected void initialize(Object[] args) {

    }

    @Override
    public Set<BotTrigger> runAction(Map<TriggersRange, String> args) {
        if (args == null) {
            LOG.error("Invalid arguments of GIFGetterService");
            return Collections.emptySet();
        }

        BotTriggerPics botTriggerGIF = new BotTriggerPics(args);

        return new HashSet<BotTrigger>() {
            {
                for (String answerToSet: botTriggerGIF.pics) {
                    add(new BotTriggerText(args){{
                        this.answer = answerToSet;
                        this.additionalComment = botTriggerGIF.picComment;
                        this.replyOnlyIds.addAll(botTriggerGIF.replyOnlyIds);
                    }});
                }
            }
        };
    }
}
