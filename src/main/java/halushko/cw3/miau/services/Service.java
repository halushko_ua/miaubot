package halushko.cw3.miau.services;

import halushko.cw3.miau.additional.MiauException;
import halushko.cw3.miau.additional.bt.BotTrigger;

import java.util.Set;

public abstract class Service<T, A> {
    Service(Object[] args) {
        initialize(args);
    }

    protected abstract void initialize(Object[] args);
    public abstract T runAction(A args) throws MiauException;
}
