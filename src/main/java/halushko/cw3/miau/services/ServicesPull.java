package halushko.cw3.miau.services;

import halushko.cw3.miau.additional.MiauException;

import java.util.HashMap;
import java.util.Map;

public class ServicesPull {
    private static Map<Class<? extends Service>, Service> services = new HashMap<Class<? extends Service>, Service>() {{
        put(TriggersGetter.class, new TriggersGetter(null));
        put(URLReaderService.class, new URLReaderService(null));
        put(GIFGetterService.class, new GIFGetterService(null));
    }};

    public static Service getService(Class<? extends Service> actionType) throws MiauException {
        if (services.containsKey(actionType)) {
            return services.get(actionType);
        }
        throw new MiauException("There is no such service: [" + String.valueOf(actionType) + "]");
    }
}
