package halushko.cw3.miau.services;

import halushko.cw3.miau.additional.*;
import halushko.cw3.miau.additional.bt.BotTrigger;
import halushko.cw3.miau.additional.bt.BotTriggerText;
import halushko.cw3.miau.bot.Controller;
import halushko.cw3.miau.utils.GoogleSheetsReader;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.*;

public class TriggersGetter extends Service<Map<String, Set<BotTrigger>>, Boolean> {
    private final static Logger LOG = Logger.getLogger(TriggersGetter.class);
    private final HashMap<String, LinkedHashSet<BotTrigger>> TRIGGERS = new HashMap<>();
    private final static String TAB_NAME = "Triggers";
    private final String CREDENTIALS_FILE_PATH = "/credentials.json";
    private final String SPREADSHEET_ID = "1eMIlegVg3kU1S9VhK1x8rAluKdXZH4uz0BU9A1DT7DE";

    private final Set<String> COLUMNS = new HashSet<String>() {{
        for (TriggersRange a : TriggersRange.values()) add(a.toString());
    }};

    TriggersGetter(Object[] args) {
        super(args);
    }

    @Override
    protected void initialize(Object[] args) {
    }

    @Override
    public Map<String, Set<BotTrigger>> runAction(Boolean args) throws MiauException {
        if (args != null && args) {
            reloadTriggers();
        }
        return getTriggers();
    }


    private void reloadTriggers() throws MiauException {
        TRIGGERS.clear();
//        if(true) throw new NullPointerException(CREDENTIALS_FILE_PATH + " " +  SPREADSHEET_ID);

        try {

            GoogleSheetsReader googleSheetsReader = new GoogleSheetsReader(CREDENTIALS_FILE_PATH, SPREADSHEET_ID);
            DataFromGoogleAPI data = new DataFromGoogleAPI(COLUMNS);

            for (Map.Entry<String, List<Object>> entry : googleSheetsReader.readRange(TAB_NAME, COLUMNS, 2).entrySet()) {
                for (int i = 0; i < entry.getValue().size(); i++) {
                    data.put(entry.getKey(), String.valueOf(entry.getValue().get(i)), i);
                }
            }

            for (int i = 0; i < data.getLength(); i++) {
                Map<TriggersRange, String> trigger = data.get(i);

                TriggerTypes triggerType = TriggerTypes.getTriggerType(trigger.get(TriggersRange.TYPE));
                triggerType = triggerType == null ? TriggerTypes.TEXT : triggerType;

                String key = trigger.get(TriggersRange.REGEXP);

                if (!TRIGGERS.containsKey(key)) {
                    TRIGGERS.put(key, new LinkedHashSet<>());
                }

                if (TriggerTypes.TEXT.equals(triggerType) || TriggerTypes.ROYALE.equals(triggerType) ) {
                    TRIGGERS.get(key).add(new BotTriggerText(data.get(i)));
                } else if (TriggerTypes.URL.equals(triggerType)) {
                    TRIGGERS.get(key).addAll((Set<BotTrigger>) ServicesPull.getService(URLReaderService.class).runAction(trigger));
                } else if (TriggerTypes.GIF.equals(triggerType) || TriggerTypes.IMAGE.equals(triggerType)) {
                    TRIGGERS.get(key).addAll((Set<BotTrigger>) ServicesPull.getService(GIFGetterService.class).runAction(trigger));
                }
            }
        } catch (IOException | GeneralSecurityException e) {
            LOG.error("Error while getting Triggers: ", e);
            throw new MiauException("Error while getting Triggers: " + e.getLocalizedMessage());
        }
    }

    private Map<String, Set<BotTrigger>> getTriggers() throws MiauException {
        return new HashMap<String, Set<BotTrigger>>() {{
            for (Entry<String, LinkedHashSet<BotTrigger>> i : TRIGGERS.entrySet()) {
                put(i.getKey(), new LinkedHashSet<>(i.getValue()));
            }
        }};
    }

}