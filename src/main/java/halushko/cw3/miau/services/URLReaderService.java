package halushko.cw3.miau.services;

import halushko.cw3.miau.additional.TriggerTypes;
import halushko.cw3.miau.additional.bt.BotTrigger;
import halushko.cw3.miau.additional.bt.BotTriggerText;
import halushko.cw3.miau.additional.bt.BotTriggerURL;
import halushko.cw3.miau.additional.MiauException;
import halushko.cw3.miau.additional.TriggersRange;
import halushko.cw3.miau.bot.Controller;
import halushko.cw3.miau.utils.URLReader;
import org.apache.log4j.Logger;

import java.util.*;

public class URLReaderService extends Service<Set<BotTrigger>, Map<TriggersRange, String>> {
    private final static Logger LOG = Logger.getLogger(URLReaderService.class);

    URLReaderService(Object[] args) {
        super(args);
    }

    @Override
    protected void initialize(Object[] args) {

    }

    @Override
    public Set<BotTrigger> runAction(Map<TriggersRange, String> args) throws MiauException {
        if (args == null) {
            LOG.error("Invalid arguments of URLReaderService");
            return Collections.emptySet();
        }

        BotTriggerURL botTriggerURL = new BotTriggerURL(args);

        return new HashSet<BotTrigger>() {
            {
                for (String answerToSet: URLReader.readFromURL(botTriggerURL.url, botTriggerURL.fromText, botTriggerURL.toText)) {
                    add(new BotTriggerText(args){{
                        this.answer = answerToSet;
                        this.type = TriggerTypes.TEXT;
                    }});
                }
            }
        };
    }
}
