package halushko.cw3.miau.utils;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.sheets.v4.Sheets;
import com.google.api.services.sheets.v4.SheetsScopes;
import com.google.api.services.sheets.v4.model.ValueRange;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.GeneralSecurityException;
import java.util.*;

public class GoogleSheetsReader {
    private static final String APPLICATION_NAME = "Google Sheets API Java Quickstart";
    private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();
    private static final String TOKENS_DIRECTORY_PATH = "tokens";

    public GoogleSheetsReader(String credentialsFilePath, String spreadsheetID) {
        this.credentialsFilePath = credentialsFilePath;
        this.spreadsheetID = spreadsheetID;
    }

    /**
     * Global instance of the scopes required by this quickstart.
     * If modifying these scopes, delete your previously saved credentials/ folder.
     */
    private static final List<String> SCOPES = Collections.singletonList(SheetsScopes.SPREADSHEETS_READONLY);
    private final String credentialsFilePath;// = "credentials.json";
    private final String spreadsheetID;// = "1eMIlegVg3kU1S9VhK1x8rAluKdXZH4uz0BU9A1DT7DE";

    /**
     * Creates an authorized Credential object.
     *
     * @param HTTP_TRANSPORT The network HTTP Transport.
     * @return An authorized Credential object.
     * @throws IOException If the credentials.json file cannot be found.
     */
    private Credential getCredentials(final NetHttpTransport HTTP_TRANSPORT) throws IOException {
        // Load client secrets.
        InputStream in = GoogleSheetsReader.class.getResourceAsStream(credentialsFilePath);
        if(in == null){
            throw new NullPointerException("Null InputStream " + credentialsFilePath + " try to reload Gradle");
        }
        GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(JSON_FACTORY, new InputStreamReader(in));

        // Build flow and trigger user authorization request.
        GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(
                HTTP_TRANSPORT, JSON_FACTORY, clientSecrets, SCOPES)
                .setDataStoreFactory(new FileDataStoreFactory(new java.io.File(TOKENS_DIRECTORY_PATH)))
                .setAccessType("offline")
                .build();
        return new AuthorizationCodeInstalledApp(flow, new LocalServerReceiver()).authorize("user");
    }

    public Map<String, List<Object>> readRange(String tabName, Set<String> columns, int fromRow) throws IOException, GeneralSecurityException {
        return readRange(tabName, columns, fromRow, -1);
    }

    public Map<String, List<Object>> readRange(String tabName, Set<String> columns, int fromRow, int toRow) throws IOException, GeneralSecurityException {
        final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
        return new HashMap<String, List<Object>>(){{
            for (String column : columns) {
                String range = tabName + "!" + column + fromRow + ":" + column + (toRow < 0 ? "" : toRow);
                Sheets service = new Sheets.Builder(HTTP_TRANSPORT, JSON_FACTORY, getCredentials(HTTP_TRANSPORT))
                        .setApplicationName(APPLICATION_NAME)
                        .build();
                ValueRange response = service.spreadsheets().values()
                        .get(spreadsheetID, range)
                        .execute();
                List<List<Object>> values = response.getValues();

                this.put(column, new ArrayList<Object>());
                for (List<Object> cell : (values == null ? new ArrayList<List<Object>>() : values)) {
                    this.get(column).add(cell == null || cell.size() < 1 ? "" : cell.get(0));
                }
            }
        }};
    }
}