package halushko.cw3.miau.utils;

import com.google.gson.Gson;
import halushko.cw3.miau.bot.royale.Clan;
import halushko.cw3.miau.bot.royale.Player;
import halushko.cw3.miau.bot.royale.War;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.util.Date;

public class RoyaleAPI {
    private final static Logger LOG = Logger.getLogger(RoyaleAPI.class);

    private static String clanURL(String tag) {
        return "https://api.royaleapi.com/clan/" + tag;
    }
    private static String playerURL(String tag) {
        return "https://api.royaleapi.com/player/" + tag;
    }
    private static String warURL(String tag) {
        return "https://api.royaleapi.com/clan/" + tag + "/war";
    }

    public static void main(String[] args){
        getClan("9PPV2PC9");
        new Date(1535303610L);
    }

    public static Clan getClan(String tag) {
        try {
            String response = send(clanURL(tag));
            if(response == null || "".equals(response)) return Clan.EMPTY;
            Gson gson = new Gson();
            Clan clan = gson.fromJson(response, Clan.class);
            response = send(warURL(tag));
            if(response == null || "".equals(response)) return clan;

            War war = gson.fromJson(response, War.class);
            for(Player player: war.participants){
                clan.getMember(player.tag).battlesPlayed = player.battlesPlayed;
            }

            return clan;
//        } catch (IOException e) {
//            LOG.error("Error while getting clan: ", e);
//            return Clan.EMPTY;
        } catch (Exception e) {
            LOG.error("Error while getting clan: ", e);
            return Clan.EMPTY;
        }

    }

    public static Player getWar(String tag) {
        try {
            String response = send(playerURL(tag));
        } catch (IOException e) {
            LOG.error("Error while getting Player: ", e);
        }
        return Player.EMPTY;
    }

    public static Player getPlayer(String tag) {
        try {
            String response = send(playerURL(tag));
        } catch (IOException e) {
            LOG.error("Error while getting Player: ", e);
        }
        return Player.EMPTY;
    }

    private static String send(String url) throws IOException {
        OkHttpClient client = new OkHttpClient();

        Request request = new Request.Builder()
                .url(url)
                .get()
                .addHeader("auth", "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MTU2NywiaWRlbiI6IjQ4MzIxNTg4NTczNDExNzM3NiIsIm1kIjp7fSwidHMiOjE1MzUyNzkzMjAwOTd9.Jh0T41bYmXZ0-CjDHIdbxWkEdJD1TOySWBqUKEYjSY8")
                .build();

        Response response = client.newCall(request).execute();
        String result = response.body() == null ? "Empty body" : response.body().string();
        LOG.debug(result);
        return result;
    }
}
