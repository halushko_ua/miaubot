package halushko.cw3.miau.utils;

import org.apache.log4j.Logger;
import org.jsoup.Jsoup;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;

public class URLReader {
    private final static Logger LOG = Logger.getLogger(URLReader.class);

    private static final String delimer = "DIMADIMADIMADIMADIMA";

    public static List<String> readFromURL(String urlAddress, String startFrom, String endWith) {
        LOG.debug("Run URL Reader");
        URL url = null;

        try {
            url = new URL(urlAddress);
        } catch (MalformedURLException e) {
            LOG.error("Не могу прочитать страницу [" + urlAddress + "]", e);
            return Collections.emptyList();
        }
        StringBuilder sb = new StringBuilder();
        try (BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream(), "UTF-8"))) {
            String line;
            while ((line = in.readLine()) != null) {
                sb.append("\n").append(line);
            }
            String html = sb.toString().replaceAll("\\<\\/p\\>\\<p\\>", delimer);
//            LOG.debug("HTML file is: " + html);
            String text = Jsoup.parse(html).text();
            if(text == null) return Collections.emptyList();

//            LOG.debug("Cut text: ");
            int i = text.indexOf(startFrom);
            text = text.substring(i < 0 ? 0 : i);
            i = text.indexOf(endWith);
            text = text.substring(0, i < 0 ? text.length() - 1 : i);
//            LOG.debug(text);
            String finalText = text;
            return new ArrayList<String>() {{
                for(String abzac: finalText.split(delimer)){
                    String[] predlozeniya = abzac.split("\\.+");
                    if(predlozeniya.length < 6){
                        this.add(abzac);
                    } else {
                        StringBuilder sb = new StringBuilder("...");
                        for(int i = new Random().nextInt(predlozeniya.length-5), j = 0; j<5; ++i, j++){
                            sb.append(predlozeniya[i]).append(".");
                        }
                        sb.append("...");
                        this.add(sb.toString());
                    }
                }
            }};
        } catch (IOException e) {
            LOG.error("Не могу прочитать строку со страницы [" + urlAddress + "]", e);
            return Collections.emptyList();
        }
    }
}
